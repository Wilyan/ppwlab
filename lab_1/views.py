from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = ['Mohammad Wildan Yanuar', 'Muhammad Faishal Ammar Wibowo', 'Wall'] # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = [date(1999, 1, 27), date(1999, 1, 27), date(1993, 1, 27)] #TODO Implement this, format (Year, Month, Date)
npm = [1706043973, 1706043973, 000000000] # TODO Implement this
hobby = ['sleeping', 'reading', 'standing']
# Create your views here.
def index(request):
    response = {'name0': mhs_name[0], 'age0': calculate_age(birth_date[0].year), 'npm0': npm[0], 'hobby0' : hobby[0],
                'name1': mhs_name[1], 'age1': calculate_age(birth_date[1].year), 'npm1': npm[1], 'hobby1' : hobby[1],
                'name2': mhs_name[2], 'age2': calculate_age(birth_date[2].year), 'npm2': npm[2], 'hobby2' : hobby[2]}
    return render(request, 'homepage.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
