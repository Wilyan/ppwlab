from django.shortcuts import render
from django.http import HttpResponse


def homepage(request):
    return render(request, "Homepage.html", {})

def portofolio(request):
    return render(request, "Portofolio.html", {})

def guestbook(request):
    return render(request, "Guest-book.html", {})
