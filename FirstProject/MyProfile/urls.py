from django.conf.urls import url
from . import views
app_name = "MyProfile"
urlpatterns = [
    url(r'^$', views.homepage, name='homepage'),
    url(r'^homepage/$', views.homepage, name='homepage'),
    url(r'^portofolio/$', views.portofolio, name="portofolio"),
    url(r'^guestbook/$', views.guestbook, name="guestbook"),
]
